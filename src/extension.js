// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const {window, workspace, commands} = require('vscode');
const shopunity = require('./tools/shopunity')
const npm = require('./tools/npm')
const debounce = require('lodash.debounce')
const includes = require('lodash.includes')
const isEmpty = require('lodash.isempty')
const filter = require('lodash.filter')
const endsWith = require('lodash.endswith')

let created = []
let deleted = []
const deNotification = debounce((codenameChanged) => {
    window.showInformationMessage('Changed File Project: '+codenameChanged);
}, 1000)
const deCreateNotification = debounce((codenameChanged) => {
    window.showInformationMessage('Create File in Project:  '+codenameChanged);
}, 1000)

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    let fileSystemWatcher = workspace.createFileSystemWatcher('**', false, false, false);

    fileSystemWatcher.onDidChange((uri) => {
        if(shopunity.checkIgnored(uri) && !includes(created, uri.fsPath) && !endsWith(uri.fsPath, '___jb_tmp___')) {
            const codenameChanged = npm.detectCodenameFromURI(uri)
            if(codenameChanged) {
                deNotification(codenameChanged)
                const workspaces = npm.getWorkspacesWithProject(codenameChanged, uri)
                if(!isEmpty(workspaces)) {
                    npm.copyFileToWorkspaces(uri, codenameChanged, workspaces)
                }
            }
        }
    })

    fileSystemWatcher.onDidCreate((uri) => {
        if(shopunity.checkIgnored(uri) && !includes(created, uri.fsPath) && !endsWith(uri.fsPath, '___jb_tmp___')) {

            const npmCodenameChanged = npm.detectCodenameFromURI(uri)
            if(npmCodenameChanged) {
                deCreateNotification(npmCodenameChanged)
                const workspaces = npm.getWorkspacesWithProject(npmCodenameChanged, uri)
                if(!isEmpty(workspaces)) {
                    npm.copyFileToWorkspaces(uri, npmCodenameChanged, workspaces)
                }
            }

            const codenameChanged = shopunity.detectCodenameFromURI(uri)

            if(codenameChanged) {
                window.showInformationMessage('Create File in Project: '+codenameChanged);
                const workspaces = shopunity.getWorkspacesWithProject(codenameChanged, uri)
                
                if(!isEmpty(workspaces)) {
                    var result = shopunity.copyFileToWorkspaces(uri, codenameChanged, workspaces)
                    for(var key in result) {
                        created.push(result[key])
                    }
                }
                
            } else {
                shopunity.updateJsonByCreate(uri, (codename) => {
                    window.showInformationMessage('Changed JSON config in Project: '+codenameChanged);

                    const workspaces = shopunity.getWorkspacesWithProject(codename, uri)
                
                    if(!isEmpty(workspaces)) {
                        var result = shopunity.copyFileToWorkspaces(uri, codename, workspaces)
                        for(var key in result) {
                            created.push(result[key])
                        }
                    }
                })
            }
        } else {
            created = filter(created, (value) => {
                return value !== uri.fsPath
            })
        }

    })
    
    fileSystemWatcher.onDidDelete((uri) => {

        if(shopunity.checkIgnored(uri)  && !includes(deleted, uri.fsPath)) {
            const codenameChanged = shopunity.detectCodenameFromURI(uri)
            if(codenameChanged) {
                window.showInformationMessage('Deleted File in Project: '+codenameChanged);
                const workspaces = shopunity.getWorkspacesWithProject(codenameChanged, uri)
                
                if(!isEmpty(workspaces)) {
                    var result = shopunity.deleteFileToWorkspaces(uri, codenameChanged, workspaces)
                    for(var key in result) {
                        deleted.push(result[key])
                    }
                }
            }
        } else {
            deleted = filter(deleted, (value) => {
                return value !== uri.fsPath
            })
        }


    })
    


    context.subscriptions.push(fileSystemWatcher);
    workspace.onDidSaveTextDocument(e => {
        const codenameChanged = shopunity.detectCodenameFromURI(e.uri)
        if(codenameChanged) {
            window.showInformationMessage('Changed File Project: '+codenameChanged);
            const workspaces = shopunity.getWorkspacesWithProject(codenameChanged, e.uri)
            if(!isEmpty(workspaces)) {
                shopunity.copyFileToWorkspaces(e.uri, codenameChanged, workspaces)
            }
        }
    });

   
    let disposable = commands.registerCommand('extension.updateGitIgnore', () => {
        shopunity.updateGitignore(() => {
            window.showInformationMessage('.gitignore updated!');
        });
    });
    
    context.subscriptions.push(disposable);


}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;