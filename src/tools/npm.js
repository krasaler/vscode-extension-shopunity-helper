const {workspace, window, extensions} = require('vscode');
const fs = require('fs')
var isWin = process.platform === 'win32'
const delimiter = isWin ? '\\' : '/'
const includes = require('lodash.includes')
const isEmpty = require('lodash.isempty')
const filter = require('lodash.filter')
const forEach = require('lodash.foreach')
const isObject = require('lodash.isobject')
const map = require('lodash.map')
const replace = require('lodash.replace')
const startsWith = require('lodash.startswith')
const join = require('lodash.join')
const split = require('lodash.split')
var copydir = require('copy-dir')

exports.getConfig = function (workspaceFolder) {
    let configData = {}
    if (fs.existsSync(workspaceFolder.uri.fsPath  +delimiter+ 'package.json')) {
        const data = fs.readFileSync(workspaceFolder.uri.fsPath  + delimiter + 'package.json');
        configData = JSON.parse(data);
    }

    return configData
}

exports.includesFileInProject = function (workspaceFolder, codename, uri) {
    const configData = this.getConfigByCodename(workspaceFolder, codename)
    if (configData) {
        var files = isObject(configData.files) ? configData.files : [];
        const relativeFilePath = replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')
   
        files = map(files, (file) => (file.split('/').join(delimiter)))
        if (includes(files, relativeFilePath)) {
            return true
        }

        var dirs = isObject(configData.dirs) ? configData.dirs : [];
        dirs = map(dirs, (dir) => (dir.split('/').join(delimiter)))
        for(var key in dirs) {
            if(startsWith(relativeFilePath, dirs[key]+delimiter)){
                return true
            }
        }

    }
    return false
}

exports.detectCodenameFromURI = function (uri) {
    var result = ''
    let workspaceFolder = workspace.getWorkspaceFolder(uri)

    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const enable = configuration.get('enableNpm');
    if (enable) {
        const config = this.getConfig(workspaceFolder)
        if(config) {
            result = config.name
        }
    }

    return result
}

exports.getWorkspacesWithProject = function (codename, uri) {
    var result = []
    let workspaceFolder = workspace.getWorkspaceFolder(uri)
    let workspaces = workspace.workspaceFolders
    forEach(workspaces, (value) => {
        if (fs.existsSync(value.uri.fsPath + delimiter+'node_modules'+delimiter+codename+delimiter +  'package.json')) {
            result.push(value)
        }
    })
    result = filter(result, (value) => (value.name !== workspaceFolder.name))

    return result
}

exports.getRelativePath = function(workspaceFolder, uri) {
    const configData = this.getConfig(workspaceFolder)
    if (configData) {
        return replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')

    }
    return false
}

exports.copyFileToWorkspaces = function (uri, codename, workspaces) {
    var result = []
    let workspaceFolder = workspace.getWorkspaceFolder(uri)

    const relativeFilePath = this.getRelativePath(workspaceFolder, uri)

    const stat = fs.statSync( workspaceFolder.uri.fsPath+delimiter+relativeFilePath)

    if (stat.isFile()) {
      forEach(workspaces, value => {
       fs.copyFileSync(
         uri.fsPath,
         value.uri.fsPath +
           delimiter +
           'node_modules' +
           delimiter +
           codename +
           delimiter +
           relativeFilePath
       )
       result.push(
         value.uri.fsPath +
           delimiter +
           'node_modules' +
           delimiter +
           codename +
           delimiter +
           relativeFilePath
       )
      })
    } else {
      forEach(workspaces, value => {
          copydir.sync(
            uri.fsPath,
            value.uri.fsPath +
              delimiter +
              'node_modules' +
              delimiter +
              codename +
              delimiter +
              relativeFilePath
          )
          result.push(
            value.uri.fsPath +
              delimiter +
              'node_modules' +
              delimiter +
              codename +
              delimiter +
              relativeFilePath
          )
      })
    }
    return result
}

exports.getConfigByWorkspaceFolder = function(workspaceFolder, uri) {
    var result = []
    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const codename = configuration.get('extension_id');

    if (codename) {
        result.push(codename)
        const configData = this.getConfigByCodename(workspaceFolder, codename);
        if (configData) {
            var dependencies = isObject(configData.required) ? configData.required : {};
            for(var key in dependencies) {
                result.push(key)
            }
        }
    }
    return result
}

exports.checkIgnored = function (uri) {
    let configuration = workspace.getConfiguration('shopunity-helper', uri);
    const ignored = configuration.get('ignored');

    const result = filter(map(ignored, (value) => {
        return includes(uri.fsPath, value)
    }), (value) =>(value === true))

    return isEmpty(result)
}


exports.updateGitignore = function(cb) {
    let gitExt = extensions.getExtension('vscode.git');

    let options = gitExt.exports._model.repositories

    options = map(options, (value) => {
        let workspaceFolder = workspace.getWorkspaceFolder(value._sourceControl._rootUri)

        let configuration = workspace.getConfiguration('shopunity-helper', value._sourceControl._rootUri);
        const extension_id = configuration.get('extension_id');
        const configData = this.getConfigByCodename(workspaceFolder, extension_id)

        return {
            label:configData.name,
            description: configData.description,
            codename: extension_id,
            repository: value,
            configData: configData
        }
    })


	this.pickFolder(options, 'Select a Project for update .gitignore').then((option) => {
		if (!option) {
			return;
        }

        forEach(option.repository.workingTreeGroup.resourceStates, (value) => {
            if(value.letter == 'U'){
                let detectCodename = this.detectCodenameFromURI(value._resourceUri)

                if(detectCodename !== option.codename) {
                    let workspaceFolder = workspace.getWorkspaceFolder(option.repository._sourceControl._rootUri)
                    let content = replace(value._resourceUri.fsPath, workspaceFolder.uri.fsPath + '\\', '')
                    content = join(split(content, '\\'), '/')
                    fs.appendFileSync(workspaceFolder.uri.fsPath +  '/.gitignore', content+'\r\n')
                }
            }
        })
        cb()
        return option
    });
}