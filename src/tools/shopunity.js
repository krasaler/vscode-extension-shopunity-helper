const {workspace, window, extensions} = require('vscode');
const fs = require('fs')
var copydir = require('copy-dir');
const includes = require('lodash.includes')
const isEmpty = require('lodash.isempty')
const filter = require('lodash.filter')
const forEach = require('lodash.foreach')
const isObject = require('lodash.isobject')
const map = require('lodash.map')
const replace = require('lodash.replace')
const startsWith = require('lodash.startswith')
const toString = require('lodash.tostring')
const size = require('lodash.size')
const join = require('lodash.join')
const split = require('lodash.split')

var isWin = process.platform === 'win32'
const delimiter = isWin ? '\\' : '/'
const shopunity_path = '/system/library/d_shopunity/extension/'


exports.getConfigByCodename = function (workspaceFolder, codename) {
    let configData = {}
    if (fs.existsSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json')) {
        const data = fs.readFileSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json');
        configData = JSON.parse(data);
    }

    return configData
}

exports.includesFileInProject = function (workspaceFolder, codename, uri) {
    const configData = this.getConfigByCodename(workspaceFolder, codename)
    if (configData) {
        var files = isObject(configData.files) ? configData.files : [];
        const relativeFilePath = replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')
   
        files = map(files, (file) => (file.split('/').join(delimiter)))
        if (includes(files, relativeFilePath)) {
            return true
        }

        var dirs = isObject(configData.dirs) ? configData.dirs : [];
        dirs = map(dirs, (dir) => (dir.split('/').join(delimiter)))
        for(var key in dirs) {
            if(startsWith(relativeFilePath, dirs[key]+delimiter)){
                return true
            }
        }

    }
    return false
}

exports.detectCodenameFromURI = function (uri) {
    var result = ''
    let workspaceFolder = workspace.getWorkspaceFolder(uri)

    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const codename = configuration.get('extension_id');
    const enable = configuration.get('enable');
 
    if (codename && enable) {
        if (this.includesFileInProject(workspaceFolder, codename, uri)) {
            result = codename
        } else {
            const configData = this.getConfigByCodename(workspaceFolder, codename);
            if (configData) {
                var dependencies = isObject(configData.required) ? configData.required : {};

                forEach(dependencies, (value, key) => {
                    if (this.includesFileInProject(workspaceFolder, key, uri)) {
                        result = key;
                    }
                })
            }

        }
    }

    return result
}

exports.getWorkspacesWithProject = function (codename, uri) {
    var result = []
    let workspaceFolder = workspace.getWorkspaceFolder(uri)
    let workspaces = workspace.workspaceFolders
    forEach(workspaces, (value) => {
        if (fs.existsSync(value.uri.fsPath + shopunity_path + codename + '.json')) {
            result.push(value)
        }
    })
    result = filter(result, (value) => (value.name !== workspaceFolder.name))

    return result
}

exports.getRelativePath = function(codename, workspaceFolder, uri) {
    const configData = this.getConfigByCodename(workspaceFolder, codename)
    if (configData) {
        var files = isObject(configData.files) ? configData.files : [];
        const relativeFilePath = replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')
        files = map(files, (file) => (file.split('/').join(delimiter)))
        if (includes(files, relativeFilePath)) {
            return relativeFilePath
        }

        var dirs = isObject(configData.dirs) ? configData.dirs : [];
        dirs = map(dirs, (dir) => (dir.split('/').join(delimiter)))
        for(var key in dirs) {
            if(startsWith(relativeFilePath, dirs[key])){
                return dirs[key]
            }
        }

    }
    return false
}

exports.copyFileToWorkspaces = function (uri, codename, workspaces) {
    var result = []
    let workspaceFolder = workspace.getWorkspaceFolder(uri)

    const relativeFilePath = this.getRelativePath(codename, workspaceFolder, uri)
    
    const stat = fs.statSync( workspaceFolder.uri.fsPath+delimiter+relativeFilePath)

    if(stat.isFile()){
        forEach(workspaces, (value) => {
            fs.copyFileSync(uri.fsPath, value.uri.fsPath + '/' + relativeFilePath);
            result.push(value.uri.fsPath + delimiter + relativeFilePath)
        })
    } else {
        forEach(workspaces, (value) => {
            copydir.sync(workspaceFolder.uri.fsPath+delimiter+relativeFilePath, value.uri.fsPath + '/'+relativeFilePath);
            result.push( value.uri.fsPath + delimiter+relativeFilePath)
        })
    }
    return result
}

exports.getConfigByWorkspaceFolder = function(workspaceFolder, uri) {
    var result = []
    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const codename = configuration.get('extension_id');

    if (codename) {
        result.push(codename)
        const configData = this.getConfigByCodename(workspaceFolder, codename);
        if (configData) {
            var dependencies = isObject(configData.required) ? configData.required : {};
            for(var key in dependencies) {
                result.push(key)
            }
        }
    }
    return result
}

exports.updateJsonByCreate = function(uri, cb){
    let workspaceFolder = workspace.getWorkspaceFolder(uri)
    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const update_json = configuration.get('update_json');

    if(!update_json) {
        return false
    }

    let options = this.getConfigByWorkspaceFolder(workspaceFolder, uri)

    options = map(options, (value) => {
        const configData = this.getConfigByCodename(workspaceFolder, value)

        return {
            label:configData.name,
            description: configData.description,
            codename: value,
            configData: configData
        }
    })
	this.pickFolder(options, 'Select a Project for add new file').then((option) => {
		if (!option) {
			return;
        }
        let relativeFilePath = replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')
        relativeFilePath=  relativeFilePath.split(delimiter).join("/")

        this.writeFileToConfig(relativeFilePath, workspaceFolder, option.codename)
        const workspaces = this.getWorkspacesWithProject(option.codename, uri)
        
        const relativeFilePathJson = shopunity_path+option.codename+'.json'
    
        forEach(workspaces, (value) => {
            fs.copyFileSync(workspaceFolder.uri.fsPath+delimiter+relativeFilePathJson, value.uri.fsPath + '/' + relativeFilePathJson);
        })

        cb(option.codename)
        return option
    });
}

exports.writeFileToConfig = function(file, workspaceFolder, codename){
    if (fs.existsSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json')) {
        let content = fs.readFileSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json');
        var original  = toString(content).match(/"files"[\s\n\r]*:[\s\n\r]*\[\s*/);
        var spaces  = toString(content).match(/"files"[\s\n\r]*:[\s\n\r]*\[\s*\n(\s*)/);
        if (!isEmpty(original) && size(spaces) > 1) {
            content = replace(content, /"files"[\s\n\r]*:[\s\n\r]*\[\s*/, original[0]+'"'+file+'",\n'+spaces[1])
            fs.writeFileSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json', content);
        }

    }
}

exports.removeFileFromConfig = function(file, workspaceFolder, codename){
    if (fs.existsSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json')) {
        let content = fs.readFileSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json');
        const filePath = file.split(delimiter).join("[\\/\\\\]")
        var regex2 = new RegExp('\\n*(\\s*"'+filePath+'",)');
        content = replace(content, regex2, '')
        fs.writeFileSync(workspaceFolder.uri.fsPath + shopunity_path + codename + '.json', content);
    }
}

exports.pickFolder = function(options, placeHolder) {
	if (options.length === 1) {
		return Promise.resolve(options[0]);
	}
	return window.showQuickPick(options,{ placeHolder: placeHolder }).then((selected) => {
		if (!selected) {
			return undefined;
        }
        return selected
	});
}

exports.deleteFileToWorkspaces = function (uri, codename, workspaces) {
    var result = []
    let workspaceFolder = workspace.getWorkspaceFolder(uri)

    let relativeFilePath = replace(uri.fsPath, workspaceFolder.uri.fsPath + delimiter, '')
    
    forEach(workspaces, (value) => {
        if(fs.existsSync(value.uri.fsPath + '/' + relativeFilePath)){
            fs.unlinkSync(value.uri.fsPath + '/' + relativeFilePath);
            result.push(value.uri.fsPath + delimiter + relativeFilePath)
        }
    })

    let configuration = workspace.getConfiguration('shopunity-helper', workspaceFolder.uri);
    const update_json = configuration.get('update_json');

    if(!update_json) {
        return result
    }

    this.removeFileFromConfig(relativeFilePath, workspaceFolder, codename)

    const relativeFilePathJson = shopunity_path+codename+'.json'

    forEach(workspaces, (value) => {
        fs.copyFileSync(workspaceFolder.uri.fsPath+relativeFilePathJson, value.uri.fsPath + relativeFilePathJson);
    })
    return result
}

exports.checkIgnored = function (uri) {
    let configuration = workspace.getConfiguration('shopunity-helper', uri);
    const ignored = configuration.get('ignored');

    const result = filter(map(ignored, (value) => {
        return includes(uri.fsPath, value)
    }), (value) =>(value === true))

    return isEmpty(result)
}


exports.updateGitignore = function(cb) {
    let gitExt = extensions.getExtension('vscode.git');

    let options = gitExt.exports._model.repositories

    options = map(options, (value) => {
        let workspaceFolder = workspace.getWorkspaceFolder(value._sourceControl._rootUri)

        let configuration = workspace.getConfiguration('shopunity-helper', value._sourceControl._rootUri);
        const extension_id = configuration.get('extension_id');
        const configData = this.getConfigByCodename(workspaceFolder, extension_id)

        return {
            label:configData.name,
            description: configData.description,
            codename: extension_id,
            repository: value,
            configData: configData
        }
    })


	this.pickFolder(options, 'Select a Project for update .gitignore').then((option) => {
		if (!option) {
			return;
        }

        forEach(option.repository.workingTreeGroup.resourceStates, (value) => {
            if(value.letter == 'U'){
                let detectCodename = this.detectCodenameFromURI(value._resourceUri)

                if(detectCodename !== option.codename) {
                    let workspaceFolder = workspace.getWorkspaceFolder(option.repository._sourceControl._rootUri)
                    let content = replace(value._resourceUri.fsPath, workspaceFolder.uri.fsPath + '\\', '')
                    content = join(split(content, '\\'), '/')
                    fs.appendFileSync(workspaceFolder.uri.fsPath +  '/.gitignore', content+'\r\n')
                }
            }
        })
        cb()
        return option
    });
}